class CreateSubcats < ActiveRecord::Migration
  def change
    create_table :subcats do |t|
      t.integer :name

      t.timestamps
    end
  end
end
