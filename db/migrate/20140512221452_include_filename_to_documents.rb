class IncludeFilenameToDocuments < ActiveRecord::Migration
  def self.up
    add_attachment :documents, :file_name
  end

  def self.down
    remove_attachment :documents, :file_name
  end
end
