class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :Title
      t.text :Content
      t.date :Updated

      t.timestamps
    end
  end
end
