class RenameFileToFilename < ActiveRecord::Migration
  def change
    rename_column :documents, :file, :file_name
  end
end
