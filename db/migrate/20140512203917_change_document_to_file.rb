class ChangeDocumentToFile < ActiveRecord::Migration
  def change
    rename_column :documents, :document, :file
  end
end
