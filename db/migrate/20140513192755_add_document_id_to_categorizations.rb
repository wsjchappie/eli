class AddDocumentIdToCategorizations < ActiveRecord::Migration
  def change
    add_column :categorizations, :document_id, :integer
  end
end
