class AddSortIdToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :sort_id, :decimal
  end
end
