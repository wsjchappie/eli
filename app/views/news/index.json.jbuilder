json.array!(@news) do |news|
  json.extract! news, :id, :Title, :Content, :Updated
  json.url news_url(news, format: :json)
end
