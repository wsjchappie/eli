class Category < ActiveRecord::Base
  has_many :categorizations
  has_many :documents, :through => :categorizations
  has_many :visibilities
  has_many :groups, :through => :visibilities

  def self.newest
       cat_list = {}
       cat_query = " select distinct
                      public.documents.id as document_id, 
                      public.categories.id as category_id,
                      public.categories.name as category_name,
                      public.categories.parent_id
                      FROM  public.documents 
                      INNER JOIN public.categorizations ON public.documents.id = public.categorizations.document_id
                      INNER JOIN public.categories ON public.categories.id = public.categorizations.category_id
                      INNER JOIN public.visibilities ON public.categories.id = public.visibilities.category_id
                      INNER JOIN public.groups ON public.groups.id = public.visibilities.group_id"                 
      @categories = Document.all.find_by_sql(cat_query) 
      cathash = []
      @categories.each do |category| 
        if  cathash["#{category.category_name}"] 
            cathash["#{category.category_name}"]  = cathash["#{category.category_name}"]  +  1
        else
            cathash["#{category.category_name}"]  = 1
        end
      end
    cathash
  end


end
