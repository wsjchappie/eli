class Document < ActiveRecord::Base
  has_many :categorizations
  has_many :categories, :through => :categorizations
  has_attached_file :file_name

 # validates_attachment_presence :file_name
  validates_attachment_content_type :file_name, :content_type => 
    ['image/jpg',
    'image/jpeg',
    'image/pjpeg',
    'image/png',
    'image/x-png',
    'image/gif',
    'application/pdf',
    'application/msword',
    'applicationvnd.ms-word',
    'applicaiton/vnd.openxmlformats-officedocument.wordprocessingm1.document',
    'application/msexcel','application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/mspowerpoint','application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'text/plain']
end
